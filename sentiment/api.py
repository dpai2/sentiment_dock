from transformers import pipeline
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()

class Data(BaseModel):
    st: str

def loadModel():
    global sentiment_task

    model_path = "cardiffnlp/twitter-xlm-roberta-base-sentiment"
    sentiment_task = pipeline("sentiment-analysis", model=model_path, tokenizer=model_path)
    # test predict
    text = 'เยี่ยมมากเลย'
    result = sentiment_task(text)

loadModel()

async def predict(data):
    text = data.st
    result = sentiment_task(text)
    return result[0]['label'], result[0]['score']

@app.post("/getclass/")
async def get_class(data: Data):
    category, confidence = await predict(data)
    res = {"class": category, "confidence": confidence}
    return {"results": res}
